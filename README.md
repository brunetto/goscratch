# GoScratch

Small template for a go app requiring network in a Go container generated from the scratch image. 
It is necessary that the binary is statically built.    
For Go you can achieve this with:

`CGO_ENABLE=0 go build -a -installsuffix cgo`
