FROM scratch

COPY cacert.pem /etc/ssl/certs/cacert.pem
COPY yourconf.json /yourconf.json
COPY youbinary /yourbinary

CMD ["/yourbinary"]